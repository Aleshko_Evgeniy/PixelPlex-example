import './scss/base.scss';
window.$ = window.jQuery = require("jquery");


function showIphone(id){
    $(".iphone-map").hide();
    $(id).show();
}

$(document).ready(() => {
    $(".polyParking").mouseover(() => {
        $(".parking").show();
        showIphone("#iphone-parking");
    });
    $(".polyParking").mouseout(() => {
        $(".parking").hide();
    });
});

$(document).ready(() => {
    $(".polyBank").mouseover(() => {
        $(".bank").show();
        showIphone("#iphone-bank");
    });
    $(".polyBank").mouseout(() => {
        $(".bank").hide();
    });
});

$(document).ready(() => {
    $(".polyMuseum").mouseover(() => {
        $(".museum").show();
        showIphone("#iphone-museum");
    });
    $(".polyMuseum").mouseout(() => {
        $(".museum").hide();
    });
});

$(document).ready(() => {
    $(".polyShop").mouseover(() => {
        $(".shop").show();
        showIphone("#iphone-shop");
    });
    $(".polyShop").mouseout(() => {
        $(".shop").hide();
    });
});

$(document).ready(() => {
    $(".polyCafe").mouseover(() => {
        $(".cafe").show();
        showIphone("#iphone-cafe");
    });
    $(".polyCafe").mouseout(() => {
        $(".cafe").hide();
    });
});

$(document).ready(() => {
    $(".polyStop").mouseover(() => {
        $(".stop").show();
        showIphone("#iphone-stop");
    });
    $(".polyStop").mouseout(() => {
        $(".stop").hide();
    });
});